package com.jeecg.teamrank.main.entity;

import java.io.Serializable;
import java.util.Date;
import java.math.BigDecimal;

/**
 * 描述：</b>JpTeamPerson:<br>
 * 实体定义规则
 * 字段不允许存在基本类型，必须都是包装类型(因为基本类型有默认值)
 * 基本数据类型  包装类 byte Byte boolean Boolean short Short char Character int Integer long Long float Float double  Double 
 * @author p3.jeecg
 * @since：2016年07月14日 10时10分49秒 星期四 
 * @version:1.0
 */
public class JpTeamPerson implements Serializable{
	private static final long serialVersionUID = 1L;
		/**	 *id	 */	private String id;	/**	 *创建人名称	 */	private String createName;	/**	 *创建人登录名称	 */	private String createBy;	/**	 *创建日期	 */	private Date createDate;	/**	 *更新人名称	 */	private String updateName;	/**	 *更新人登录名称	 */	private String updateBy;	/**	 *更新日期	 */	private Date updateDate;	/**	 *所属部门	 */	private String sysOrgCode;	/**	 *所属公司	 */	private String sysCompanyCode;	/**	 *名称	 */	private String name;	/**	 *头像路径	 */	private String imgSrc;	/**	 *简介	 */	private String introduction;	/**	 *加入时间	 */	private Date jionDate;	/**	 *是否参与；1为是，0为否	 */	private Integer isJoin;	public String getId() {	    return this.id;	}	public void setId(String id) {	    this.id=id;	}	public String getCreateName() {	    return this.createName;	}	public void setCreateName(String createName) {	    this.createName=createName;	}	public String getCreateBy() {	    return this.createBy;	}	public void setCreateBy(String createBy) {	    this.createBy=createBy;	}	public Date getCreateDate() {	    return this.createDate;	}	public void setCreateDate(Date createDate) {	    this.createDate=createDate;	}	public String getUpdateName() {	    return this.updateName;	}	public void setUpdateName(String updateName) {	    this.updateName=updateName;	}	public String getUpdateBy() {	    return this.updateBy;	}	public void setUpdateBy(String updateBy) {	    this.updateBy=updateBy;	}	public Date getUpdateDate() {	    return this.updateDate;	}	public void setUpdateDate(Date updateDate) {	    this.updateDate=updateDate;	}	public String getSysOrgCode() {	    return this.sysOrgCode;	}	public void setSysOrgCode(String sysOrgCode) {	    this.sysOrgCode=sysOrgCode;	}	public String getSysCompanyCode() {	    return this.sysCompanyCode;	}	public void setSysCompanyCode(String sysCompanyCode) {	    this.sysCompanyCode=sysCompanyCode;	}	public String getName() {	    return this.name;	}	public void setName(String name) {	    this.name=name;	}	public String getImgSrc() {	    return this.imgSrc;	}	public void setImgSrc(String imgSrc) {	    this.imgSrc=imgSrc;	}	public String getIntroduction() {	    return this.introduction;	}	public void setIntroduction(String introduction) {	    this.introduction=introduction;	}	public Date getJionDate() {	    return this.jionDate;	}	public void setJionDate(Date jionDate) {	    this.jionDate=jionDate;	}	public Integer getIsJoin() {	    return this.isJoin;	}	public void setIsJoin(Integer isJoin) {	    this.isJoin=isJoin;	}
}

